FROM debian 

RUN apt update 
RUN apt install curl wget apache2 apache2-utils -y
RUN apt clean 
RUN a2enmod ssl
RUN a2ensite default-ssl

WORKDIR /var/www/html
COPY . .

EXPOSE 80 443
CMD ["apache2ctl","-D","FOREGROUND"]