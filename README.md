# About
This is example "hello world" webapp. It contains simple HTML template with some Javascript.   
Purpose of this app is to have "something" to clone into droplets, containers, etc. and to demonstrate basic functionality.

## Bored?
Let's play a game! Just clone this repo into Document root of your webserver.  

![Lets play a game!](images/screenshot.png "Where's Waldo?")

Credits for the magnifier code go to [Shantanu Jana a.k.a. Foolish Developer](https://www.foolishdeveloper.com/2022/05/image-magnifier-javascript.html).

## Run using image from this repository
### Docker
```bash
docker run -d -p 80:80 -p 443:443 --restart unless-stopped --name  hello_waldo registry.gitlab.com/janjansmid/hello-waldo
```
### Kubernetes Helm Chart
```bash
helm repo add hello-waldo https://gitlab.com/api/v4/projects/41894328/packages/helm/latest
helm repo update
helm install hello-waldo hello-waldo/hello-waldo-helm-chart --namespace hello-waldo --create-namespace
```

## Build and Run locally
### Docker
```bash
docker build -t hello_waldo:1.0 .
docker run -d -p 80:80 -p 443:443 --restart unless-stopped --name  hello_waldo hello_waldo:1.0

docker stop hello_waldo && docker rm hello_waldo

```
### Docker compose
```bash
docker-compose up -d
docker-compose down
```

### Run Insecure Local Docker registry
[DockerHub](https://registry.hub.docker.com/_/registry)
```bash
docker run -d -p 5000:5000 --restart always --name registry registry:2

# DNS - edit IP to match your server's IP and run on all registry clients or setup local DNS server.
echo '1.2.3.4 registry.og' >> /etc/hosts

#Push Hello_Waldo image into repository
docker tag hello-world-website_apache registry.og:5000/hello-waldo
docker push registry.og:5000/hello-waldo

#Try it:
docker run -d -p 80:80 -p 443:443 --restart unless-stopped --name  hello_waldo registry.local:5000/hello-waldo
docker ps
docker stop hello_waldo && docker rm hello_waldo
```

### K3S Cluster setup for insecure registry
Run on all cluster nodes
```bash
echo '192.168.210.222 registry.og' >> /etc/hosts
mkdir -p /etc/rancher/k3s
tee -a /etc/rancher/k3s/registries.yaml > /dev/null <<EOT
mirrors:
  "registry.og:5000":
    endpoint:
      - "http://registry.og:5000"
EOT
#on worker nodes
systemctl restart k3s-agent.service
#on master nodes
systemctl restart k3s.service

crictl info
```

### Kompose
```bash
#install kompose
curl -L https://github.com/kubernetes/kompose/releases/download/v1.27.0/kompose-linux-amd64 -o kompose
chmod +x kompose
sudo mv ./kompose /usr/local/bin/kompose

#symlink just for better names in helm chart, it's taken from file name.
ln -s docker-compose.yml hello-waldo-helm-chart.yml

#generate helm chart
kompose convert -c -f hello-waldo-helm-chart.yml
```

### Deploy Helm chart
```bash
# change image name
sed -i 's/image: apache/image: registry.og:5000\/hello-waldo/g' ./hello-waldo-helm-chart/templates/apache-deployment.yaml

# create ingress
tee -a ./hello-waldo-helm-chart/templates/apache-ingress.yaml > /dev/null <<EOT
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: apache-ingress
  annotations:
    kubernetes.io/ingress.class: "traefik"
spec:
  rules:
  - host: hello-waldo.kube.local
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: apache
            port:
              number: 80
EOT

# DNS - change to IP of your kubernetes cluster and edit local hosts file, or setup local DNS
echo '4.3.2.1 hello-waldo.kube.local' >> /etc/hosts

# deploy local helm chart
helm upgrade --install hello-waldo ./hello-waldo-helm-chart --namespace hello-waldo --create-namespace

# delete helm chart
helm uninstall hello-waldo
```
### Package and publish helm chart
```bash
# Package helm chart
helm package hello-waldo-helm-chart

# install helm-push plugin
helm plugin install https://github.com/chartmuseum/helm-push

# push to repository
helm repo add --username jan******* --password glpat-******** hello-waldo https://gitlab.com/api/v4/projects/41894328/packages/helm/latest
helm cm-push hello-waldo-helm-chart-0.0.1.tgz hello-waldo

# install helm chart from repository
helm repo add hello-waldo https://gitlab.com/api/v4/projects/41894328/packages/helm/latest
helm repo update
helm install hello-waldo hello-waldo/hello-waldo-helm-chart --namespace hello-waldo --create-namespace

```
test
